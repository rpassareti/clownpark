﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnemy : MonoBehaviour {

    public GameObject[] enemy, objAnim;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn()
    {
        for (int i = 0; i < enemy.Length; i++)
        {
            if (i < objAnim.Length)
            {
                objAnim[i].GetComponent<Animator>().SetTrigger("Actv");
            }
            yield return new  WaitForSeconds(0.1f);
            enemy[i].GetComponent<enemyAI>().state = enemyAI.States.Appear;
            
        }
    }
}
