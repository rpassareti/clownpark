﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAI : MonoBehaviour {

    public enum States
    {
        Desactivated,
        Appear,
        Stay,
        Die
    }
    public States state = States.Appear;

    public int life = 2;

    public AudioClip[] clownSound;
    public GameObject movePosition;

    private GameObject aim, player;
    public List<GameObject> model;
    private bool playSound;

    void Awake()
    {
        aim = transform.GetChild(0).gameObject;

        MeshRenderer[] mr = transform.GetChild(1).GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer go in mr)
        {
            if(go.enabled)
            {
                model.Add(go.gameObject);
                go.enabled = false;
            }
        }

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;

        player = GameObject.FindWithTag("Player");

        gameMaster.Instance.Resume += doResume;
        gameMaster.Instance.Resume += doPause;
        
    }

    public void doResume()
    {
        Resume(true);
    }

    void Resume(bool actv)
    {
        switch (state)
        {
            case States.Desactivated:
                break;
            case States.Appear:
                AppearState();
                break;
            case States.Stay:
                StayState();
                break;
            case States.Die:
                DieState();
                break;
            default:
                break;
        }
    }

    void AppearState()
    {
        foreach (GameObject obj in model)
        {
            obj.GetComponent<MeshRenderer>().enabled = true;
        }

        //model.GetComponent<MeshRenderer>().enabled = true;
        //GetComponent<MeshRenderer>().enabled = true;
        GetComponent<BoxCollider>().enabled = true;

        if (!playSound)
        {
            int r = Random.Range(0, clownSound.Length);
            GetComponent<AudioSource>().PlayOneShot(clownSound[r]);
            playSound = true;
        }

        transform.position = Vector3.Lerp(transform.position, movePosition.transform.position, Time.deltaTime * 2);
        if (Vector3.Distance(transform.position, movePosition.transform.position) < 0.1f)
        {
            state = States.Stay;
        }
    }

    void StayState()
    {
        Vector3 targetPoint = new Vector3(player.transform.position.x, aim.transform.position.y, player.transform.position.z) - aim.transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(targetPoint, Vector3.up);
        aim.transform.rotation = Quaternion.Slerp(aim.transform.rotation, targetRotation, Time.deltaTime * 2.0f);

        // shot

        if (life <= 0)
        {
            //anim
            player.GetComponent<playerController>().pontuacao += 5;
            state = States.Die;
        }
    }

    void DieState()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        //model.GetComponent<MeshRenderer>().enabled = false;
        foreach (GameObject obj in model)
        {
            obj.GetComponent<MeshRenderer>().enabled = false;
        }
        this.enabled = false;

    }

    public void doPause()
    {
        Pause(true);
    }

    void Pause(bool actv)
    {
        if (actv == true)
        {
            return;
        }
    }
}
