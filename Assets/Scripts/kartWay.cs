﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kartWay : MonoBehaviour {

    public enum States
    {
        Menu,
        Move,
        Gameover,
        End
    }
    public States state = States.Menu;

    public Transform[] waypoints;
    private Vector3 velocity = Vector3.zero;
    public Quaternion iniRotation;

    public Animator anim;

    public int currentWayPoint;

    public float speed = 5;
    public float speedFinal;
    public float rotSpeed = 40;

    public BezierCurve bc;

    public GameObject[] bp;
    public Vector3 relativeWayPointPos;

    void Awake()
    {
            gameMaster.Instance.Resume += doResume;
            gameMaster.Instance.Resume += doPause;
    }

    public void StartGame()
    {
        state = States.Move;
    }

    public void doResume()
    {
        Resume(true);
    }

    void Resume(bool actv)
    {

        switch (state)
        {
            case States.Menu:
                break;
            case States.Move:
                Move();
                break;
            case States.Gameover:
                break;
            case States.End:
                break;
            default:
                break;
        }
    }
    public void doPause()
    {
        Pause(true);
    }

    void Pause(bool actv)
    {
        if (actv == true)
        {
            return;
        }
    }

    public void Move()
    {
        if (speedFinal <= 1)
        {
            speedFinal += Time.deltaTime / speed;

            Vector3 _direction = (bc.GetPointAt(speedFinal) - transform.position).normalized;

            Quaternion _lookRotation = Quaternion.LookRotation(_direction);

            transform.rotation = Quaternion.Slerp(transform.rotation,
                                 _lookRotation,
                                 Time.deltaTime * rotSpeed);

            GetComponent<Rigidbody>().MovePosition(bc.GetPointAt(speedFinal));
        }
        else
        {
            speedFinal = 1;
        }
    }
}
