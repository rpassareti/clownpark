﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour {

    private GameObject aim;
    private int bullets = 20;
    private float timeBullet = 0;

    public float velocity = 15;
    public int timeLimit = 1;
    public int pontuacao;

    public Text bulletTxt;
    public Text pontTxt;
    public Image loadBullet;
    public AudioClip[] audioThrow;

    void Awake()
    {
        gameMaster.Instance.Resume += doResume;
        gameMaster.Instance.Resume += doPause;

        aim = GameObject.Find("Kart/Cardboard/Player/Main Camera/Aim");
    }

    public void doResume()
    {
        Resume(true);
    }

    void Resume(bool actv)
    {
        bulletTxt.text = bullets.ToString();
        pontTxt.text = pontuacao.ToString();
        if (bullets > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameObject ballon = Instantiate((GameObject)Resources.Load("ballon", typeof(GameObject)), aim.transform.position, aim.transform.rotation);
                ballon.GetComponent<Rigidbody>().AddForce(aim.transform.forward * velocity, ForceMode.VelocityChange);
                bullets--;
                int r = Random.Range(0, audioThrow.Length - 1);
                aim.GetComponent<AudioSource>().PlayOneShot(audioThrow[r]);
            }
        }

        if(bullets < 20)
        {
            timeBullet += Time.deltaTime;
            loadBullet.fillAmount = timeBullet / timeLimit;
            if(timeBullet > timeLimit)
            {
                bullets++;
                timeBullet = 0;
                loadBullet.fillAmount = 0;
            }
        }
    }
    public void doPause()
    {
        Pause(true);
    }

    void Pause(bool actv)
    {
        if (actv == true)
        {
            return;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Finish")
        {
            GameObject.Find("Kart/Cardboard/Player/Globo_Fade").GetComponent<objectFade>().StartFadeOut();
        }
    }
}
