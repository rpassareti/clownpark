﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class objectFade : MonoBehaviour
{
    /// <summary>
    /// How long it takes to fade.
    /// </summary>
    public float fadeTime = 2.0f;

    /// <summary>
    /// The initial screen color.
    /// </summary>
    public Color fadeColorIn = new Color(0.01f, 0.01f, 0.01f, 1.0f);
    public Color fadeColorOut = new Color(0.01f, 0.01f, 0.01f, 0.0f);

    public Material fadeMaterial = null;
    private bool isFading = false;
    private YieldInstruction fadeInstruction = new WaitForEndOfFrame();
    private string videoName;


    /// <summary>
    /// Initialize.
    /// </summary>
    void Awake()
    {
        // create the fade material
        fadeMaterial = GetComponent<MeshRenderer>().material;
        Color alpha = fadeMaterial.color;
        alpha.a = 1;
        fadeMaterial.color = alpha;
    }

    /// <summary>
    /// Starts the fade in
    /// </summary>
    void OnEnable()
    {
        StartCoroutine(FadeIn());
    }

    /// <summary>
    /// Starts a fade in when a new level is loaded
    /// </summary>
    void OnLevelWasLoaded(int level)
    {
        StartCoroutine(FadeIn());
    }

    /// <summary>
    /// Cleans up the fade material
    /// </summary>
    void OnDestroy()
    {
        if (fadeMaterial != null)
        {
            Destroy(fadeMaterial);
        }
    }

    /// <summary>
    /// Fades alpha from 1.0 to 0.0
    /// </summary>
    IEnumerator FadeIn()
    {  
        yield return new WaitForSeconds(1f);

        float elapsedTime = 0.0f;
        fadeMaterial.color = fadeColorIn;
        Color color = fadeColorIn;
        isFading = true;
        while (elapsedTime < fadeTime)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            color.a = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
            fadeMaterial.color = color;
        }

        isFading = false;

        
        
    }

    IEnumerator FadeOut()
    {
        float elapsedTime = 0.0f;
        fadeMaterial.color = fadeColorOut;
        Color color = fadeColorOut;
        isFading = true;
        while (elapsedTime < fadeTime)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            color.a = 0.0f + Mathf.Clamp01(elapsedTime / fadeTime);
            fadeMaterial.color = color;
        }
        isFading = false;
        gameMaster.Instance.gameOver();

        //StartFadeIn();
    }

    public void StartFadeIn()
    {
        StartCoroutine(FadeIn());
    }
    public void StartFadeOut()
    {
        StartCoroutine(FadeOut());
    }
}
