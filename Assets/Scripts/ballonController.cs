﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballonController : MonoBehaviour {

    float timer;

    void Awake()
    {
        gameMaster.Instance.Resume += doResume;
        gameMaster.Instance.Resume += doPause;
    }

    private void OnDestroy()
    {
        gameMaster.Instance.Resume -= doResume;
        gameMaster.Instance.Resume -= doPause;
    }

    public void doResume()
    {
        Resume(true);
    }

    void Resume(bool actv)
    {
        timer += Time.deltaTime;

        if(timer > 5)
            Destroy(this.gameObject);
    }

    public void doPause()
    {
        Pause(true);
    }

    void Pause(bool actv)
    {
        if (actv == true)
        {
            return;
        }
    }



    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "enemy")
        {
            Debug.Log("enemy");
            Debug.Log(collision.transform.name);
            //Destroy(collision.gameObject);
            collision.transform.GetComponent<enemyAI>().life--;
        }
        else
        {
            Debug.Log("otherthing:" + collision.transform.name);
        }
        Destroy(this.gameObject);
    }
}
