﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameMaster : MonoBehaviour
{
    private enum gameStatus
    {
        Pause,
        Resume,
        GameOver,
        EndGame
    }


    public delegate void OnPause();
    public event OnPause Pause;
    public delegate void OnResume();
    public event OnResume Resume;
    public delegate void OnGameOver();
    public event OnGameOver GameOver;


    private static gameMaster instance;
    public static gameMaster Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(gameMaster)) as gameMaster;
                if (instance == null)
                {
                    GameObject gameMaster = Instantiate(Resources.Load("Prefebs/GameMaster")) as GameObject;
                    instance = gameMaster.GetComponent<gameMaster>();
                }
            }
            return instance;
        }
    }
    gameStatus status = gameStatus.Pause;


    public float playerLife = 5;
    

    void OnApplicationQuit()
    {
        gameMaster.Instance.Pause -= doPause;
        gameMaster.Instance.Resume -= doResume;
        gameMaster.Instance.GameOver -= doGameOver;
    }

    void Awake()
    {
        gameMaster.Instance.Pause += doPause;
        gameMaster.Instance.Resume += doResume;
        gameMaster.Instance.GameOver += doGameOver;
    }
    public void pause()
    {
        status = gameStatus.Pause;
        Pause();
    }

    private void doPause()
    {
        return;
    }

    public void resume()
    {
        status = gameStatus.Resume;
        Resume();
    }

    private void doResume()
    {
        if (playerLife == 0)
            status = gameStatus.GameOver;
    }
    public void gameOver()
    {
        status = gameStatus.GameOver;
        GameOver();
    }
    private void doGameOver()
    {
        SceneManager.LoadScene(0);
    }

    void Update()
    {
        if (status == gameStatus.Resume)
        {
            resume();
        }
        else if (status == gameStatus.Pause)
        {
            pause();
        }
        else if (status == gameStatus.GameOver)
        {
            gameOver();
        }
    }
}
